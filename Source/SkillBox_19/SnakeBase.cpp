// Fill out your copyright notice in the Description page of Project Settings.
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 80.f;
	MovementSpeed = .3f;
	HotDuration = 40.f;
	HotIntensity = 1.5f;
	MovementRadius = 8;
	LastMoveDirection = EMovementDirection::LEFT;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);

	AddSnakeElement(5);
	CanMove = true;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

	if (HotRemainder > 0) {
		HotRemainder -= DeltaTime;
		if (HotRemainder <= 0) {
			SetActorTickInterval(MovementSpeed);
		}
	}
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) {

		//FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FVector NewLocation(0, 0, 0);
		if (!SnakeElements.IsEmpty()) NewLocation = SnakeElements.Last()->GetActorLocation();
		FTransform NewTransform(NewLocation);

		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0) {
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::RemoveSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) {
		if (SnakeElements.IsValidIndex(1)) {
			SnakeElements.Pop()->Destroy();
		}
		//Change to handle death
		else this->Destroy();		
	}
}

void ASnakeBase::SetHot(float HotDurationMass)
{
	HotRemainder = HotDuration * HotDurationMass;
	SetActorTickInterval(MovementSpeed/HotIntensity);
}

void ASnakeBase::Move()
{
	FVector MovementVector;

	switch (LastMoveDirection) {
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();


	for (int i = SnakeElements.Num() - 1; i > 0; --i) {
		auto CurElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurElement->SetActorLocation(PrevLocation);
	}

	//Movement in bounds
	SnakeElements[0]->AddActorWorldOffset(MovementVector);;

	if ((SnakeElements[0]->GetActorLocation().X) > (ElementSize * (MovementRadius - 1)))
		SnakeElements[0]->SetActorLocation(FVector(SnakeElements[0]->GetActorLocation().X - 2 * ElementSize * MovementRadius,
												  SnakeElements[0]->GetActorLocation().Y,
												  0));
	if ((SnakeElements[0]->GetActorLocation().X) < (-1 * ElementSize * (MovementRadius - 1)))
		SnakeElements[0]->SetActorLocation(FVector((SnakeElements[0]->GetActorLocation().X + 2 * ElementSize * MovementRadius),
			SnakeElements[0]->GetActorLocation().Y,
			0));
	if ((SnakeElements[0]->GetActorLocation().Y) > (ElementSize * (MovementRadius - 1)))
		SnakeElements[0]->SetActorLocation(FVector(SnakeElements[0]->GetActorLocation().X,
			SnakeElements[0]->GetActorLocation().Y - 2 * ElementSize * (MovementRadius),
			0));
	if ((SnakeElements[0]->GetActorLocation().Y) <  (-1 * ElementSize * (MovementRadius - 1)))
		SnakeElements[0]->SetActorLocation(FVector(SnakeElements[0]->GetActorLocation().X,
			SnakeElements[0]->GetActorLocation().Y + 2 * ElementSize * MovementRadius,
			0));

	SnakeElements[0]->ToggleCollision();

	CanMove = true;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);
		if(InteractableInterface) {
			InteractableInterface->Interact(this,bIsFirst);
		}
	}
}

