// Fill out your copyright notice in the Description page of Project Settings.


#include "Poison.h"
#include "SnakeBase.h"
#include "Spawner.h"

void APoison::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->RemoveSnakeElement();
			if (Owner) Owner->Despawn();
			else this->Destroy();
		}
	}
}
