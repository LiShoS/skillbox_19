// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "FoodBase.generated.h"

class ASpawner;

UCLASS()
class SKILLBOX_19_API AFoodBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodBase();

	ASpawner* Owner;

	UPROPERTY(EditAnywhere)
	float LifeDuration;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float TimeBefoteDeath;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	void SetAlive(float LifeCicle);
};
