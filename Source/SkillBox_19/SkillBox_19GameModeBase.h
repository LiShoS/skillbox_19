// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkillBox_19GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SKILLBOX_19_API ASkillBox_19GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
