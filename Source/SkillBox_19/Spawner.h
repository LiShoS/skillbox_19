// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

class AFoodBase;

UCLASS()
class SKILLBOX_19_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	UPROPERTY(EditAnywhere)
	TSubclassOf<AFoodBase> ToSpawnClass;

	UPROPERTY(EditAnywhere)
	float FoodLifeDuration;

	UPROPERTY(EditAnywhere)
	float SpawnInterval;

	UPROPERTY(EditAnywhere)
	int32 MaxSpawned;

	UPROPERTY()
	TArray<AFoodBase*> Spawned;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float RemainedBeforeSpawn;
	float RemainedBeforeDeath;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void NewSpawn();

	UFUNCTION()
	void Despawn();
};
