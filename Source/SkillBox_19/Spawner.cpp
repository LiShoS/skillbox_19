// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "FoodBase.h"


// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	RemainedBeforeSpawn = 0.f;
	if (SpawnInterval > 0) RemainedBeforeSpawn = SpawnInterval;
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (SpawnInterval == 0) NewSpawn();

	if (RemainedBeforeSpawn > 0) {
		RemainedBeforeSpawn -= DeltaTime;
		if (RemainedBeforeSpawn <= 0) {
			NewSpawn();
			RemainedBeforeSpawn = SpawnInterval;
		}
	}
}

void ASpawner::NewSpawn()
{
	if ((Spawned.Num() < MaxSpawned) && (ToSpawnClass)) {
		//������� � �������� ����
		FVector NewSpawnLocation(FMath::RandRange(-6, 6) * 60.f, FMath::RandRange(-6, 6) * 60.f, 0);
		
		AFoodBase* NewFood = GetWorld()->SpawnActor<AFoodBase>(ToSpawnClass, FTransform(NewSpawnLocation));
		NewFood->Owner = this;
		if (FoodLifeDuration > 0) NewFood->SetAlive(FoodLifeDuration);
		Spawned.Add(NewFood);
	}
}

void ASpawner::Despawn()
{
	if (!Spawned.IsEmpty()) {
		Spawned.Pop()->Destroy();
		RemainedBeforeSpawn = SpawnInterval;
	}
}

