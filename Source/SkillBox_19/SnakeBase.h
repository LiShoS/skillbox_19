// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SKILLBOX_19_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
	float HotDuration;

	UPROPERTY(EditDefaultsOnly)
	float HotIntensity;

	UPROPERTY(EditDefaultsOnly)
	int32 MovementRadius;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

	bool CanMove;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int32 HotRemainder;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION()
	void RemoveSnakeElement(int ElementsNum = 2);

	UFUNCTION()
	void SetHot(float HotDurationMass = 1.f);

	UFUNCTION()
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor);
};
