// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FoodBase.h"
#include "Poison.generated.h"

/**
 * 
 */
UCLASS()
class SKILLBOX_19_API APoison : public AFoodBase
{
	GENERATED_BODY()
	
public:
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
