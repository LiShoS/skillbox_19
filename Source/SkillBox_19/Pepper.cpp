// Fill out your copyright notice in the Description page of Project Settings.


#include "Pepper.h"
#include "SnakeBase.h"
#include "Spawner.h"

void APepper::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->AddSnakeElement();
			Snake->SetHot();
			if (Owner) Owner->Despawn();
			else this->Destroy();
		}
	}
}
